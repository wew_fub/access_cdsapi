# Access CDSAPI
WORK   IN   PROGRESS.
Please refer to the climate data store (CDS): https://cds.climate.copernicus.eu
The atmosphere data store (ADS, https://ads.atmosphere.copernicus.eu/#!/home) is **not** fully supported, yet.

This collection of two scripts does the following:
1. **ecmwf_parameters** is a tool to search or pick an item from the ecmwf catalogue of geophysical parameters. E.g. search "cape" (ecmwf_parameters.search('cape')) yields ALL entries which have the string "cape" somewhere in their names. If you get "cape" (ecmwf_parameters.get('cape')) this will only give you the entry where it matches identically.
2. access_cdsapi is a tool to make a request and send this request to the cdsapi.

## Usage
Import by running: 
```
from access_cdsapi import access_cdsapi
```

To make a request, just call "get_flexible_cds_request" a year, month, day and hours. The default request will download single-level forecast field for the following variables: total_column_water_vapour, 2m_temperature, 2m_dewpoint_temperature, 10m_u_wind_component, 10m_v_wind_component, surface_pressure, skin_temperature, sea_surface_temperature
```
my_request=access_cdsapi.get_flexible_cds_request(2017,9,20,hour=[0,6,12,18],product_type='fc')
my_filename=access_cdsapi.get_filename(my_request,store_dir='/home/user/data/ecmwf_an/')
access_cdsapi.make_request(my_request,filename=my_filename)

```

To access profile fields, you have to provide other variables (i.e., ozone mass mixing ratio, temperature) and also define the levels you want to get the data at.
```
my_request_pl=access_cdsapi.get_flexible_cds_request(2023,9,29,hour=[0,6,12,18],
                                variables=['temperature','specific_humidity','ozone_mass_mixing_ratio'],
                                levels=access_cdsapi.cds_default_levels)
my_filename_pl=access_cdsapi.get_filename(my_request_pl,store_dir='/home/jrelkassar/data/ecmwf_fc/')
access_cdsapi.make_request(my_request_pl,filename=my_filename_pl)

```
The default type is forecast (fc), you may switch to reanalysis (an) to get the equivalent analysis. 


## Support
For problems, write at jan.elkassar@met.fu-berlin.de

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
This project will be updated from time to time.
