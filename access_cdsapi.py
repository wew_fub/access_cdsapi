#!/usr/bin/python3
'''
This script makes access to the cdsapi more straight-forward (in my humble opinion).

usage: 
'''
__author__     = "Jan El Kassar"
__contact__    = "jan.elkassar@wew.fu-berlin.de"
__copyright__  = "Copyright 2024"
__license__    = ""
__maintainer__ = "Jan El Kassar"
__version__    = "0.9.0"
__date__       = "24/01/2024"     

import os,sys
from itertools import product
from datetime import datetime,timedelta
import cdsapi
c=cdsapi.Client()
DIR, FILENAME = os.path.split(__file__)
sys.path.append(DIR)
import ecmwf_parameters

ecmwf_default_levels=[1, 2, 3, 5, 7, 10, 20, 30, 50, 70, 100, 150, 200, 250, 300, 400, 500, 600, 700, 800, 850, 900, 925, 950, 1000]
cds_default_levels=[1, 2, 3, 5, 7, 10, 20, 30, 50, 70, 100, 125, 150, 175, 200, 225, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 775, 800, 825, 850, 875, 900, 925, 950, 975, 1000]

def get_reanalysis_cds_request(year,month,day,
                       time=[0,3,6,9,12,15,18,21],
                       area=[-180,180,-90,90],
                       resolution=.5,
                       variables=['specific_humidity','temperature','ozone_mass_mixing_ratio'],
                       pressure_levels=cds_default_levels,
                    ):
    '''
    
    '''
    
    request={
        'year':str(year),
        'month':str(month),
        'day':str(day),
        'time':['%02i:00'%t for t in time],
        'variable':variables,        
        'grid':[resolution,resolution],# lon/lat resolution
        'area':[area[3],area[0],area[2],area[1]], # N/E/S/W area boundaries
        'product_type': 'reanalysis',
        'format':'netcdf'
    }
    
    if pressure_levels is not None:
        request['pressure_level']=[str(l) for l in pressure_levels]
    return request

def get_flexible_cds_request(year=2000,month=1,day=1,
                            date=None,
                            hour=[0,3,6,9,12,15,18,21],
                            area=[-180,180,-90,90],
                            resolution=.5,
                            product_type='fc',
                            variables=['total_column_water_vapour','2m_temperature','2m_dewpoint_temperature','10m_u_wind_component','10m_v_wind_component','surface_pressure','skin_temperature','sea_surface_temperature'],
                            levels=None,
                            fc_hour=6,
                            ):
    '''
    EITHER provide integer,list of integer for year,month,day 
    OR     provide string (YYYY-MM-DD), list of strings for date
    hour: list of hours, in the case of fc, these are time steps after the fc_init_time (=fc_hour)
    area: [lon_min,lon_max,lat_min,lat_max]
    resolution:
    product_type: fc for forecast, re/an for analysis
    variabels:
    levels: None for single levels, list of integers for pressure levels  

    fc_hour: None, default will be set to 6
    '''

    if date is None:
        year=[year] if not isinstance(year,list) else year
        month=[month] if not isinstance(month,list) else month
        day=[day] if not isinstance(day,list) else day
        date = ['%04i-%02i-%02i'%date for date in (product(*[year,month,day]))] 
    else:
        date = [date] if not isinstance(date,list) else date
        
    request={'class':'ea',
            'stream':'oper',
            'expver': '1',
            'format':'netcdf',
            'date':  date    
            }
    if product_type in ['fc','forecast']:
        request['type']='fc'
        request['time']=['%02i:00'%fc_hour]
        request['step']='/'.join(['%i'%h for h in hour])
        
    elif product_type in ['an','re','analysis','reanalysis']:
        request['type']='an'
        request['time']=['%02i:00'%h for h in hour]
        
        
    if levels is None:
        request['levtype']='sfc'
    else:
        request['levtype']='pl'
        request['levelist']='/'.join(['%i'%l for l in levels])
        
    request['param']='/'.join(['%s'%ecmwf_parameters.get(v).param_id for v in variables])
    request['area']=[area[3],area[0],area[2],area[1]]
    request['grid']=[resolution, resolution]

    return request

def get_filename(request,extra_id='',store_dir='.'):
    if 'class' in request:
        if request['class'] in ['ea']:
            basename='era5'+extra_id
        else:
            basename=''+extra_id
        type=request['type']
    else:
        basename='era5'+extra_id
        type='an'
    if 'date' in request:
        date=request['date']
    elif request['product_type']=='reanalysis':
        date=['-'.join([request[datetyp] for datetyp in ['year','month','day']])]
        # request['date']=date
    else:
        date='-'.join(['.'.join(request[datetyp]) for datetyp in ['year','month','day']])
        # request['date']=date
        
    start,stop=[datetime.strptime(d,'%Y-%m-%d') for d in date[::len(date)-1]] if len(date)>1 else [datetime.strptime(d,'%Y-%m-%d') for d in [date[0],date[0]]]
    
    if 'step' in request:
        start=start+timedelta(hours=int(request['time'][0].split(':')[0]))+timedelta(hours=int(request['step'].split('/')[0]))
        stop=stop+timedelta(hours=int(request['time'][-1].split(':')[0]))+timedelta(hours=int(request['step'].split('/')[-1]))
    else:
        start=start+timedelta(hours=int(request['time'][0].split(':')[0]))
        stop=stop+timedelta(hours=int(request['time'][-1].split(':')[0]))
    if 'levtype' in request:
        levtype=request['levtype']
        levels=0 if levtype=='sfc' else len(request['levelist'].split('/'))
    else:
        if 'pressure_level' in request:
            levtype='pl'
            levels=len(request['pressure_level'])
        else:  
            levtype='sfc'
            levels=0
    return os.path.join(store_dir,"%s_%s_%s_%s_%s_r%.02f_l%02i.nc"%(basename,levtype,type,start.strftime('%Y%m%dT%H'),stop.strftime('%Y%m%dT%H'),request['grid'][0],levels))

available_cds_datasets=['reanalysis-era5-complete','reanalysis-era5-pressure-levels','reanalysis-era5-single-levels']

def make_request(request,cds_dataset='reanalysis-era5-complete',filename=None,client=c,extra_id='',store_dir='.'):
    filename=get_filename(request,extra_id=extra_id,store_dir=store_dir) if filename is None else filename
    if os.path.exists(filename) and os.path.getsize(filename)>400:
        print('%s is already there!'%filename)
        return
    client.retrieve(cds_dataset,request,filename)
    

if __name__=='__main__':
    import argparse
    parser = argparse.ArgumentParser(description='''SCRIPT TO PROCESS MTG FCI LEVEL1!''')
    parser.add_argument('dir')
    parser.add_argument('--date',default='2020,6,1',help='fractional end point! 0,0 means from the start of the x/y of an array, .5/.5 only half of it!')
    parser.add_argument('--time',default='0,3,6,9,12,15,18,21',help='fractional end point! 0,0 means from the start of the x/y of an array, .5/.5 only half of it!')
    parser.add_argument('--area',default='-180,180,-90,90',help='fractional end point! 1,1 means until the end of the x/y of an array, .5/.5 only half of it!')
    parser.add_argument('--grid',default='.5,.5')
    parser.add_argument('--param_plv',default='',help='where to store the ')
    parser.add_argument('--param_sfc',default='total_column_water_vapour,2m_temperature,2m_dewpoint_temperature')
    args = parser.parse_args()