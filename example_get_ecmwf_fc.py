from access_cdsapi import access_cdsapi

# define date:
date=(2024,9,10)

# define storage place:
store_dir='/home/jrelkassar/data/ecmwf_fc/'

# define variables
variables=['total_column_water_vapour','2m_temperature','2m_dewpoint_temperature','10m_u_wind_component','10m_v_wind_component','surface_pressure','skin_temperature','sea_surface_temperature']

# define type (fc=forecast, an=reanalyis)
product_type='fc'

# build request dictionary
my_request=access_cdsapi.get_flexible_cds_request(*date,hour=[0,6,12,18],
                                                  variables=variables
                                                  product_type=product_type)

# send request (and download :) )
access_cdsapi.make_request(my_request,filename=access_cdsapi.get_filename(my_request,store_dir=store_dir))