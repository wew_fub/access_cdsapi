import os
import numpy as np
from collections import namedtuple

DIR, FILENAME = os.path.split(__file__)



def get_diff_between_lists(left,right,return_index=False):
    '''
    get what is different/missing in the right list from the left list
    '''
    elements=list(set(left) - set(right))
    elements.sort()
    if return_index:
        return [left.index(e) for e in elements]
    else:
        return elements

def get_common_between_lists(left,right,return_index=False,duplicates=False):
    '''
    get what is in the left list common between both lists
    '''
    if duplicates:
        items = set(right)
        if return_index:
            return [ix for ix,i in enumerate(left) if i in items]
        else:
            return [i for ix,i in enumerate(left) if i in items]
    else:
        elements=list(set(left).intersection(right))
        elements.sort()
        if return_index:
            return [left.index(e) for e in elements]
        else:
            return elements

def parse_ecmwf_parameter(p):
    if len(p)<=3:
        return p+'.128'
    elif len(p)==6:
        return str(int(p[3:]))+'.'+p[:3]
    else:
        return p
    
def flatten(liste):
    return [y for x in liste for y in x]
    
def find_str_index(liste,value):
    try:
        return liste.index(value)
    except:
        return None
    
def find_substr_index(substr,liste,return_value=False):
    if not return_value:
        return [i for i,l in enumerate(liste) if substr in l]
    else:
        return [l for l in liste if substr in l]
    
def check_if_spectral_range(v):
    if '(' in v and ' um)' in v:
        return True
    else:
        return False
    
def check_if_total_fine_mode(v):
    if ' < ' in v:
        return True
    else:
        return False
    
def ecmwf_longname_to_cds_name(n):
    if check_if_spectral_range(n):
        n_=n.replace(' (','_').replace(' um) ','um_').replace(' - ','-')
    elif check_if_total_fine_mode(n):
        n_=n.long_name.replace(' (r < 0.5 um)','').replace(' at ',' ').replace(' nm','nm')
    else:
        n_=n.replace(' at ',' ').replace(' nm','nm')
    if ('water vapour' in n_) and ('Total column' in n_) and ('vertically-integrated' in n_):
        n_=n_.replace(' vertically-integrated','')
    if n_.endswith('optical depth'):
        n_+=' 550nm'
    n_=n_.replace(' metre','m')
    return n_.replace('clear-sky','clear_sky').replace('time-integrated','time_integrated').replace(' ','_').lower()
    
param=namedtuple('parameter','long_name short_name cds_name unit code param_id')
long_names,short_names,units,params,grib1,grib2,netcdf=np.loadtxt(os.path.join(DIR,'ecmwf_var_param.txt'),'str',delimiter='\t').T
param_ids=[parse_ecmwf_parameter(p) for p in params]
cds_names=[ecmwf_longname_to_cds_name(long_name) for long_name in long_names]
PARAMETER_DATABASE={'long_name':list(long_names),
                   'short_name':list(short_names),
                   'cds_name':list(cds_names),
                   'unit':list(units),
                   'param':list(params),
                   'param_id':param_ids}


    

def get_ecmwf_parameter(value=None,search=None,param_database=PARAMETER_DATABASE):
    if search is not None:
        index={p:[ix for ix in find_substr_index(search,param_database[p]) if ix is not None] for p in param_database}
        return flatten([[param(*(param_database[p][ix] for p in param_database)) for ix in index[p]] for p in param_database])
    elif value is not None:
        index=[ix for ix in [find_str_index(param_database[p],value) for p in param_database] if ix is not None]
        if len(index)>1:
            return [param(*(param_database[p][ix] for p in param_database)) for ix in index]
        elif len(index)==1:
            return param(*(param_database[p][index[0]] for p in param_database))
        else:
            return None
        
def get(value):
    return get_ecmwf_parameter(value)

def search(search):
    return get_ecmwf_parameter(search=search)
        
ecmwf_param_id_levtype={'sfc': ['1.228', '10.228', '11.228', '12.228', '121.128', '122.128', '123.128', '129.128', '13.228', '131.228', '132.228', '134.128', '136.128', '137.128', '139.128', '14.228', '141.128', '142.128', '143.128', '144.128', '145.128', '146.128', '147.128', '148.128', '15.128', '151.128', '159.128', '16.128', '160.128', '161.128', '162.128', '163.128', '164.128', '165.128', '166.128', '167.128', '168.128', '169.128', '17.128', '170.128', '172.128', '173.128', '174.128', '175.128', '176.128', '177.128', '178.128', '179.128', '18.128', '180.128', '181.128', '182.128', '183.128', '186.128', '187.128', '188.128', '189.128', '195.128', '196.128', '197.128', '198.128', '20.128', '20.3', '201.128', '202.128', '205.128', '206.128', '208.128', '209.128', '21.228', '210.128', '211.128', '212.128', '213.128', '216.228', '217.228', '218.228', '219.228', '22.228', '220.228', '221.228', '222.228', '223.228', '224.228', '225.228', '226.228', '227.228', '228.128', '229.128', '23.228', '230.128', '231.128', '232.128', '234.128', '235.128', '236.128', '238.128', '24.228', '243.128', '244.128', '245.128', '246.228', '247.228', '251.228', '26.128', '26.228', '260015', '260109', '260121', '260123', '27.128', '27.228', '28.128', '28.228', '29.128', '29.228', '3.228', '30.128', '31.128', '32.128', '33.128', '34.128', '35.128', '36.128', '37.128', '38.128', '39.128', '40.128', '41.128', '42.128', '43.128', '44.128', '44.228', '45.128', '46.228', '47.128', '47.228', '48.228', '49.128', '50.128', '57.128', '58.128', '59.128', '66.128', '67.128', '7.228', '74.128', '78.128', '79.128', '8.128', '8.228', '80.228', '81.228', '82.228', '83.228', '84.228', '85.228', '88.228', '89.228', '9.128', '9.228', '90.228'], 
 'pl': ['10.228', '11.228', '12.228', '129.128', '13.228', '130.128', '131', '132', '133.128', '134.128', '135.128', '136.128', '137.128', '138.128', '139.128', '14.228', '141.128', '148.128', '15.128', '151.128', '155.128', '157.128', '16.128', '160.128', '161.128', '162.128', '163.128', '164.128', '165.128', '166.128', '167.128', '168.128', '17.128', '170.128', '172.128', '173.128', '174.128', '18.128', '183.128', '186.128', '187.128', '188.128', '198.128', '203.128', '206.128', '234.128', '235.128', '236.128', '238.128', '246.228', '247.228', '26.128', '27.128', '28.128', '29.128', '30.128', '31.128', '32.128', '33.128', '34.128', '35.128', '36.128', '37.128', '38.128', '39.128', '40.128', '41.128', '42.128', '43.128', '60.128', '66.128', '67.128', '7.228', '74.128', '8.228', '9.228']}